Using STM32F411E Discovery card with the CUBE HAL Library and the development environment "System Workbench":

  This example shows how to configure an external interrupt, as well as changing the value of a GPIO output from a logical state HIGH to LOW or LOW to HIGH.
  1 EXTI Line (EXTI Line0) is configured to generate an interrupt on each rising and falling edge, respectively.
  
  The most appropiate way to launch this example is:
  At the address: NAME_OF_PROJECT/inc and NAME_OF_PROJECT/src, replace the original files with those available in this repository. With these steps, the project can be compiled correctly.  
	
  at the file main.c:
  
  At the beginning of the main program the HAL_Init() function is called to reset all the peripherals, initialize the Flash interface and the systick.
  
  The function static void EXTILine0_Config(void) configure PA0 as input floating, enable and set EXTI Line 0 interrupt.
  
  The function void MX_GPIO_Init(void) configure PC0 as output, where SET changes the logical state to HIGH and RESET changes to LOW.
  
  The function void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) is associated with the interruption. Users have to program in this function.
  